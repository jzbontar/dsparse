ABOUT
-----

Implementation of [Highly Efficient Forward and Backward
Propagation of Convolutional Neural Networks for Pixelwise
Classification](http://arxiv.org/abs/1412.4526) in CUDA wrapped as
torch modules.

Contains two modules: SpatialConvolutionMM_dsparse and
SpatialMaxPooling_dsparse. SpatialConvolutionMM_dsparse was modified from
[torch](https://github.com/torch/cunn/blob/master/SpatialConvolutionMM.cu).
SpatialMaxPooling_dsparse was written from scratch.

INSTALL
-------

	$ make install
