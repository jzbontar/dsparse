PREFIX=$(HOME)/torch/install
CFLAGS=-I$(PREFIX)/include/THC -I$(PREFIX)/include/TH -I$(PREFIX)/include
LDFLAGS=-L$(PREFIX)/lib -Xlinker -rpath,$(PREFIX)/lib -lluaT -lTHC -lTH

all: libdsparse.so

libdsparse.so: dsparse.cu SpatialConvolutionMM.cu SpatialMaxPooling.cu
	nvcc -arch sm_35 --compiler-options '-fPIC' --shared -o libdsparse.so dsparse.cu $(LDFLAGS) $(CFLAGS)

install: libdsparse.so
	mkdir -p $(PREFIX)/share/lua/5.1/dsparse
	cp *.lua $(PREFIX)/share/lua/5.1/dsparse
	cp libdsparse.so $(PREFIX)/lib/lua/5.1/

clean:
	rm -f *.so
