require 'init'

eps = 1e-3

function testJacobian(module, input, x, dx)
   module:forward(input:clone())

   x = x or input

   local sx = torch.CudaTensor(x:storage())
   local gradInput = torch.CudaTensor():resizeAs(module.output)
   local sgradInput = torch.CudaTensor(gradInput:storage())
   local jacobian = torch.Tensor(sx:nElement(), gradInput:nElement())
   local jacobian_hat = torch.Tensor(sx:nElement(), gradInput:nElement())

   -- Build Jacobian from module's updateGradInput
   for i = 1,gradInput:nElement() do
      sgradInput:zero()
      sgradInput[i] = 1
      module:updateGradInput(input, gradInput)
      if dx then
         dx:zero()
         module:accGradParameters(input, gradInput)
         jacobian:select(2, i):copy(dx)
      else
         jacobian:select(2, i):copy(module.gradInput)
      end
   end

   -- Numerically estimate the Jacobian
   for i = 1,sx:nElement() do
      orig = sx[i]
      sx[i] = orig + eps
      module:forward(input:clone())
      local f1 = module.output:clone()

      sx[i] = orig - eps
      module:forward(input:clone())
      local f2 = module.output:clone()

      jacobian_hat:select(1, i):copy(f1:add(-1, f2):div(2 * eps))
      sx[i] = orig
   end

   return jacobian:add(-1, jacobian_hat):abs():max()
end

function testJacobianParameters(module, input)
   x, dx = module:getParameters()
   return testJacobian(module, input, x, dx)
end

function test_SpatialMaxPooling_dsparse()
   print('SpatialMaxPooling_dsparse')
   A = torch.CudaTensor(2, 3, 4, 5):normal()
   n = nn.SpatialMaxPooling_dsparse(2, 2, 1, 1, 2, 2):cuda()
   print(testJacobian(n, A))
end

function test_SpatialConvolutionMM_dsparse()
   print('SpatialConvolutionMM_dsparse')
   for i=1,20 do
      width = 20 + i
      height = 20 + 2 * i
      ksize = 2 + math.ceil(i / 7)
      skip = math.ceil(i / 5)
      A = torch.CudaTensor(2, 3, height, width):normal()
      n = nn.SpatialConvolutionMM_dsparse(3, 4, ksize, ksize, 1, 1, skip, skip):cuda()
      print(('width=%d, height=%d, ksize=%d, skip=%d'):format(width, height, ksize, skip))
      print(testJacobian(n, A))
      print(testJacobianParameters(n, A))
   end
end

test_SpatialMaxPooling_dsparse()
test_SpatialConvolutionMM_dsparse()
