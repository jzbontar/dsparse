#define TB 128

__global__ void MaxPool_forward_(const float *input, float *output, unsigned int *indices, int kernel, int stride, int skip, int c, int h_out, int w_out, int h_in, int w_in, int size)
{
	int id = blockIdx.x * blockDim.x + threadIdx.x;
	if (id < size) {
		int nn = id;
		int x_out = nn % w_out; nn /= w_out;
		int y_out = nn % h_out; nn /= h_out;
		int cc = nn % c; nn /= c;
		int x_in = x_out * stride;
		int y_in = y_out * stride;
		unsigned int max_ind = 0;
		float max_val = -INFINITY;
		for (int i = 0; i < kernel; i++) {
			for (int j = 0; j < kernel; j++) {
				int yy = y_in + i * skip;
				int xx = x_in + j * skip;
				if (yy < h_in && xx < w_in) {
					assert(0 <= yy && yy < h_in);
					assert(0 <= xx && xx < w_in);
					int ind = ((nn * c + cc) * h_in + yy) * w_in + xx;
					float val = input[ind];
					if (val > max_val) {
						max_val = val;
						max_ind = ind;
					}
				}
			}
		}
		output[id] = max_val;
		indices[id] = max_ind;
	}
}

static int cunn_SpatialMaxPooling_updateOutput(lua_State *L)
{
	THCState *state = getCutorchState(L);
	THCudaTensor *input_ = (THCudaTensor *)luaT_checkudata(L, 2, "torch.CudaTensor");
	int kW = luaT_getfieldcheckint(L, 1, "kW");
	int kH = luaT_getfieldcheckint(L, 1, "kH");
	int dW = luaT_getfieldcheckint(L, 1, "dW");
	int dH = luaT_getfieldcheckint(L, 1, "dH");
	int sW = luaT_getfieldcheckint(L, 1, "sW");
	int sH = luaT_getfieldcheckint(L, 1, "sH");

	assert(kW == kH && dW == dH && sW == sH);

	THCudaTensor *output_ = (THCudaTensor *)luaT_getfieldcheckudata(L, 1, "output", "torch.CudaTensor");
	THCudaTensor *indices_ = (THCudaTensor *)luaT_getfieldcheckudata(L, 1, "indices", "torch.CudaTensor");

	int w_in = input_->size[3];
	int h_in = input_->size[2];
	int c = input_->size[1];
	int bs = input_->size[0];

	int h_out = (double)(h_in - ((kH - 1) * sH + 1)) / dH + 1;
	int w_out = (double)(w_in - ((kW - 1) * sW + 1)) / dW + 1;

	THCudaTensor_resize4d(state, output_, bs, c, h_out, w_out);
	THCudaTensor_resize4d(state, indices_, bs, c, h_out, w_out);

	int size = THCudaTensor_nElement(state, output_);

	float *input = THCudaTensor_data(state, input_);
	float *output = THCudaTensor_data(state, output_);
	unsigned int *indices = (unsigned int *)THCudaTensor_data(state, indices_);

	MaxPool_forward_<<<(size - 1) / TB + 1, TB>>>(input, output, indices, kH, dH, sH, c, h_out, w_out, h_in, w_in, size);
	check_cuda_error();
	return 0;
}

__global__ void MaxPool_backward_input_(const float *grad_output, const unsigned int *indices, float *grad_input, int size)
{
	int id = blockIdx.x * blockDim.x + threadIdx.x;
	if (id < size) {
		atomicAdd(grad_input + indices[id], grad_output[id]);
	}
}

static int cunn_SpatialMaxPooling_updateGradInput(lua_State *L)
{
	THCState *state = getCutorchState(L);
	THCudaTensor *input = (THCudaTensor *)luaT_checkudata(L, 2, "torch.CudaTensor");
	THCudaTensor *gradOutput = (THCudaTensor *)luaT_checkudata(L, 3, "torch.CudaTensor");
	THCudaTensor *gradInput = (THCudaTensor *)luaT_getfieldcheckudata(L, 1, "gradInput", "torch.CudaTensor");
	THCudaTensor *indices = (THCudaTensor *)luaT_getfieldcheckudata(L, 1, "indices", "torch.CudaTensor");

	input = THCudaTensor_newContiguous(state, input);
	gradOutput = THCudaTensor_newContiguous(state, gradOutput);

	THCudaTensor_resizeAs(state, gradInput, input);
	THCudaTensor_zero(state, gradInput);

	unsigned int *indices_data = (unsigned int *)THCudaTensor_data(state, indices);
	float *gradInput_data = THCudaTensor_data(state, gradInput);
	float *gradOutput_data = THCudaTensor_data(state, gradOutput);

	int size = THCudaTensor_nElement(state, gradOutput);
	MaxPool_backward_input_<<<(size - 1) / TB + 1, TB>>>(gradOutput_data, indices_data, gradInput_data, size);
	check_cuda_error();
	return 0;
}

static const struct luaL_Reg cunn_SpatialMaxPooling__ [] = {
  {"SpatialMaxPooling_dsparse_updateOutput", cunn_SpatialMaxPooling_updateOutput},
  {"SpatialMaxPooling_dsparse_updateGradInput", cunn_SpatialMaxPooling_updateGradInput},
  {NULL, NULL}
};

static void cunn_SpatialMaxPooling_init(lua_State *L)
{
  luaT_pushmetatable(L, "torch.CudaTensor");
  luaT_registeratname(L, cunn_SpatialMaxPooling__, "nn");
  lua_pop(L,1);
}
