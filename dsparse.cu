#include <assert.h>

extern "C" {
    #include "lua.h"
    #include "lualib.h"
    #include "lauxlib.h"
}

#include "luaT.h"
#include "THC.h"

THCState* getCutorchState(lua_State* L)
{
    lua_getglobal(L, "cutorch");
    lua_getfield(L, -1, "getState");
    lua_call(L, 0, 1);
    THCState *state = (THCState*) lua_touserdata(L, -1);
    lua_pop(L, 2);
    return state;
}

void check_cuda_error(void) {
	assert(cudaPeekAtLastError() == 0);
	assert(cudaDeviceSynchronize() == 0);
}

#include "SpatialConvolutionMM.cu"
#include "SpatialMaxPooling.cu"

extern "C" int luaopen_libdsparse(lua_State *L) {
	cunn_SpatialConvolutionMM_init(L);
	cunn_SpatialMaxPooling_init(L);
	return 1;
}
